<?php

if (!isset($_REQUEST['profile'])) {
    $action = 'profile';
}else{
    $action = $_REQUEST['profile'];
}
switch ($action){
    case 'postreg' : {
        if (isset($_POST['email']) && isset($_POST['pseudo']) && isset($_POST['password']) ){
            if (Pdog6::getMailIfExist($_POST['email'])['email'] != $_POST['email'] && Pdog6::getPseudoIfExist($_POST['pseudo'])['pseudo']  != $_POST['pseudo']){
                $token  = '';
                for($i = 0; $i < strlen($_POST['email']); $i++) {
                    $token .= bin2hex(substr($_POST['email'], $i, 1));
                }
                Pdog6::addMember($_POST['email'], md5($_POST['password']), $_POST['pseudo'], './assets/images/users/avatar/avatar_default.jpg',$token);
                $link_validate = (string) $_SERVER['PHP_SELF'] . '/?page=registration&profile=validation' ;
                if (mail ($_POST['email'] , "Validation compte forum" , "Veuillez clicker sur ce lien pour valider votre inscription : " . $link_validate )){
                    header('Location:' . '?page=accueil');
                }
            }else{
                $_SESSION['error'] = 'Votre email ou votre pseudo existe déjà en base';
                header('Location:' . '?page=registration');
            }
        }else{
            $_SESSION['error'] = 'Veuillez renseigner tout les champs';
            header('Location:' . '?page=registration');
        }
        break;
    }
    case 'postConnect' :  {
        if (isset($_POST['email']) && isset($_POST['password'])){
            $login = $_POST['email'];
            $pass = md5($_POST['password']);
            $user = Pdog6::login($login , $pass);
            if ($user){
                $_SESSION["IDUser"] = $user[0]["id_user"];
                header('Location: ?page=accueil');
            }else{
                $_SESSION["error"] = "Votre identifiant ou votre mot de passe ne sont pas valide.";
                header('Location: ?page=login');
            }
        }else{
            $_SESSION['error'] = 'Veuillez renseigner tout les champs';
            header('Location:' . '?page=login');
        }
        break;
    }
    case 'editPass' : {
        if ($_POST['cur_password'] !== "" && $_POST['new_password'] !== ""){
            $userPass = Pdog6::getPassIfExist(md5($_POST['cur_password']));
            if ($userPass){
                Pdog6::updatePass($_SESSION["IDUser"] , md5($_POST['new_password']));
                $_SESSION['success'] = 'Votre mot de passe à été modifié';
                header('Location:' . '?page=profile');
            }else{
                $_SESSION['errorPass'] = 'Votre mot de passe ne correspond pas';
                header('Location:' . '?page=profile');
            }
        }else{
            $_SESSION['errorPass'] = 'Veuillez renseigner tout les champs';
            header('Location:' . '?page=profile');
        }
        break;
    }
    case 'editMail' : {
        if ($_POST['new_email'] !== ""){
            $userMail = Pdog6::getMailIfExist($_POST['new_email']);
            if (!$userMail){
                Pdog6::updateMail($_SESSION["IDUser"] , $_POST['new_email']);
                $_SESSION['successMail'] = 'Votre email à été modifié';
                header('Location:' . '?page=profile');
            }else{
                $_SESSION['errorMail'] = 'Votre email se trouve déja en base';
                header('Location:' . '?page=profile');
            }
        }else{
            $_SESSION['errorMail'] = 'Veuillez renseigner tout les champs';
            header('Location:' . '?page=profile');
        }
        break;
    }

}