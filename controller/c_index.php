<?php

if (!isset($_REQUEST['page'])) {
    $action = 'accueil';
}else{
    $action = $_REQUEST['page'];
}

switch ($action) {
    case 'accueil': {
        include("./view/forum.php");
        break;
    }
    case 'topic': {
		if (isset($_REQUEST['cat'])){
			$subCat = Pdog6::getSubCategory($_REQUEST['cat']);		
			include("./view/forums-topics.php");
			break;
		}
		include("./view/404.php");
		break;
    }
    case 'login':{
        if (isset($_REQUEST['profile'])){
            include ("./controller/c_profile.php");
            break;
        }
        include("./view/login.php");
        break;
    }
    case 'registration':{
        if (isset($_REQUEST['profile'])){
            include ("./controller/c_profile.php");
            break;
        }
        include("./view/registration.php");
        break;
    }
    case 'logOut':{
        unset($_SESSION["IDUser"]);
        header('Location:' . '?page=accueil');
        break;
    }
    case 'profile':{
        $user = Pdog6::getUser($_SESSION['IDUser']);
        if (isset($_REQUEST['profile'])){
            include ("./controller/c_profile.php");
        }
        include("./view/user-profile.php");
        break;
    }
	case 'subTopic': {
		if (isset($_REQUEST['sub_topic'])){
			$subTopic = Pdog6::getTopic($_REQUEST['sub_topic']);		
			include('./view/forums-sub-topics.php');
			break;
		}
		include("./view/404.php");
		break;
	}
    default: {
        include("./view/404.php");
        break;
    }

}
?>