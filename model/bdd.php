<?php
class Pdog6
{
    private static $serveur = 'mysql:host=localhost';
    //private static $bdd='dbname=V2';
    private static $bdd = 'dbname=forum';
    private static $user = 'root';
    private static $mdp = '';
    private static $utf8 = 'charset=utf8';
    private static $monPdo = null;

    /**
     * Constructeur privé, crée l'instance de PDO qui sera sollicitée
     * pour toutes les méthodes de la classe
     */
    private function __construct()
    {
        self::$monPdo = new PDO(self::$serveur . ';' . self::$bdd . ';' . self::$utf8, self::$user, self::$mdp);
    }

    public function _destruct()
    {
        self::$monPdo = null;
    }

    /**
     * Fonction statique qui crée l'unique instance de la classe
     * Appel : $instancePdo = Pdo::getPdo();
     * @return l'unique objet de la classe Pdo
     */
    public static function getPdo()
    {
        if (self::$monPdo == null) {
            new Pdog6();
        }
    }

    public static function getCategory(){
        self::getPdo();
        $req = "Select * from category";
        $rs = self::$monPdo->query($req);
        $cat = $rs->fetchAll();
        return $cat;
    }
    public static function getSubCategory($idCat){
        self::getPdo();
        $req = "Select * from sub_category WHERE id_theme = " . $idCat .";";
        $rs = self::$monPdo->query($req);
        $subCat = $rs->fetchAll();
        return $subCat;
    }
    public static function getNbrTopic($idSub){
        self::getPdo();
        $req = "Select COUNT(*) from topic WHERE id_sub = " . $idSub .";";
        $rs = self::$monPdo->query($req);
        $nbrSubCat = $rs->fetchAll();
        return $nbrSubCat;
    }
    public static function getNbrSub($idCat){
        self::getPdo();
        $req = "Select COUNT(*) from sub_category WHERE id_theme = " . $idCat .";";
        $rs = self::$monPdo->query($req);
        $nbrTopic = $rs->fetch();
        return $nbrTopic[0];
    }
    public static function getNbrPost($idTopic){
        self::getPdo();
        $req = "Select COUNT(*) from post WHERE id_sub = " . $idTopic .";";
        $rs = self::$monPdo->query($req);
        $nbrPost = $rs->fetch();
        return $nbrPost;
    }
    public static function getMailIfExist($email){
        self::getPdo();
        $req = 'Select email from profile WHERE email = "'.$email.'"';
        $rs = self::$monPdo->query($req);
        $emailE = $rs->fetch();
        return $emailE;
    }
    public static function getPseudoIfExist($pseudo){
        self::getPdo();
        $req = 'Select pseudo from profile WHERE pseudo = "'.$pseudo.'"';
        $rs = self::$monPdo->query($req);
        $pseudoE = $rs->fetch();
        return $pseudoE;
    }

    public static function addMember($email, $pass, $pseudo, $urlPhoto, $token){
        self::getPdo();
        $req = 'INSERT INTO profile  (`email`, `password`, `pseudo`, `url_photo`, `token`, `lvl`) VALUES ("'.$email.'" , "'.$pass.'", "'.$pseudo.'","'.$urlPhoto.'", "'.$token.'", 0)';
        self::$monPdo->exec($req);
    }
    public static function login($email, $pass){
        self::getPdo();
        $req = 'Select * from profile WHERE email ="'.$email.'" AND password = "'.$pass.'"';
        $rs = self::$monPdo->query($req);
        $user = $rs->fetchAll();
        return $user;
    }
    public static function getPseudo($id){
        self::getPdo();
        $req = 'Select pseudo from profile WHERE id_user ="'.$id.'"';
        $rs = self::$monPdo->query($req);
        $user = $rs->fetch();
        return $user;
    }
    public static function getUser($id){
        self::getPdo();
        $req = 'Select * from profile WHERE id_user ="'.$id.'"';
        $rs = self::$monPdo->query($req);
        $user = $rs->fetch();
        return $user;
    }
    public static function getPassIfExist($pass){
        self::getPdo();
        $req = 'Select password from profile WHERE password ="'.$pass.'"';
        $rs = self::$monPdo->query($req);
        $user = $rs->fetch();
        return $user;
    }
    public static function updatePass($id, $pass){
        self::getPdo();
        $req = 'UPDATE profile SET password = "'.$pass.'" WHERE id_user = "'.$id.'"';
        self::$monPdo->exec($req);
    }
    public static function updateMail($id, $mail){
        self::getPdo();
        $req = 'UPDATE profile SET email = "'.$mail.'" WHERE id_user = "'.$id.'"';
        self::$monPdo->exec($req);
    }
	public static function getTopic($id){
		self::getPdo();
		$req = 'select * from topic WHERE id_sub = "'.$id.'"';
		$rs = self::$monPdo->query($req);
		$topic = $rs->fetch();
		return $topic;
	}
	public static function getDetailsTopic($id){
        self::getPdo();
        $req = 'select * from proile WHERE id_user = "'.$id.'"';
        $rs = self::$monPdo->query($req);
        $details = $rs->fetch();
        return $details;
    }

}