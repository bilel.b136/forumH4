  <!-- Preloader -->
  <div class="page-preloader preloader-wrapp">
    <img src="assets/images/logo.png" alt="">
    <div class="preloader"></div>
  </div>
  <!-- /Preloader -->

  <?php include ('view/components/navbar.php');?>


  <!-- Main Content -->
  <section class="content-wrap">

    <!-- Banner -->
    <div class="youplay-banner banner-top youplay-banner-parallax xsmall">
      <div class="image" style="background-image: url('assets/images/banner-blog-bg.jpg')">
      </div>

      <div class="info">
        <div>
          <div class="container">
            <h1>Topics</h1>
          </div>
        </div>
      </div>
    </div>
    <!-- /Banner -->


    <div class="container youplay-content">

      <div class="col-md-12">

        <!-- Breadcrumb -->
        <div class="mt-10 mb-20 pull-left">
          <a href="index-2.html">Home</a>
          <span class="fa fa-angle-right"></span>
          <a href="forum.php">Forums</a>
          <span class="fa fa-angle-right"></span>
          <span>Games</span>
        </div>
        <!-- /Breadcrumb -->

        <div class="clearfix"></div>

        <!-- Forums List -->
        <ul class="youplay-forum mr-10">
          <li class="header">
            <ul>
              <li class="cell-icon"></li>
              <li class="cell-info">Topic</li>
              <li class="cell-topic-count">Voices</li>
              <li class="cell-reply-count">Posts</li>
              <li class="cell-freshness">Freshness</li>
            </ul>
          </li>

          <li class="body">

		  <?php
		  var_dump($subTopic);
			foreach($subTopic as $unTopic){
			?>
				<ul>
				  <li class="cell-icon">
					<i class="fa fa-folder-open-o"></i>
				  </li>
				  <li class="cell-info">
					<a href="#!" class="title h4"><?=$subTopic['nom']?></a>
					<div class="description">
					  Créé par:
					  <a href="#!">
						<img alt="" src="<?//=$user = Pdog6::getDetailsTopic($unTopic['id_user']); echo $user?>" height="15" width="15">Administrateur
					  </a>
					</div>
				  </li>
				  <li class="cell-topic-count"></li>
				  <li class="cell-reply-count">Réponses : 21</li>
				  <a href="#!"><?=$subTopic['subjectTopic']?></a>
				  <li class="cell-freshness">
				   <a href="#!">2 year, 9 months ago</a>
					<p>
					  <a href="#!">
						<img alt="" src="assets/images/avatar-user-1.png" height="25" width="25">Kristen Bradley
					  </a>
					</p>
				  </li>
				</ul>
			<?php
			}
			?>
        <!-- /Forums List -->

        <div class="clearfix"></div>
		
      </div> 
	  
    </div>