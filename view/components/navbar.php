
<!-- Navbar -->
<nav class="navbar-youplay navbar navbar-default navbar-fixed-top ">
    <div class="container">
        <div class="navbar-header">

        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <a class="navbar-brand" href="index-2.html">
                    <img src="assets/images/logo.png" alt="">
                </a>

                <li class="dropdown dropdown-hover active">
                    <a href="#!" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        Je sais pas quoi mettre <span class="caret"></span> <span class="label">full list</span>
                    </a>
                    <div class="dropdown-menu">
                        <ul role="menu">
                            <li class="dropdown dropdown-submenu pull-left ">
                                <a href="#!" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">User</a>
                                <div class="dropdown-menu">
                                    <ul role="menu">
                                        <li><a href="user-activity.html">Activity</a>
                                        </li>
                                        <li><a href="user-profile.html">Profile</a>
                                        </li>
                                        <li><a href="user-messages.html">Messages</a>
                                        </li>
                                        <li><a href="user-messages-compose.html">Messages Compose</a>
                                        </li>
                                        <li><a href="user-settings.html">Settings</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="active"><a href="?page=accueil">Forums</a>
                            </li>
                            <li><a href="?page=topics">Forums Topics</a>
                            </li>
                            <li><a href="forums-single-topic.html">Single Topic</a>

                        </ul>
                    </div>
                </li>
            </ul>
            <?php if (isset($_SESSION["IDUser"])){
                ?>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown dropdown-hover">
                        <a href="#!" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <?= Pdog6::getPseudo($_SESSION["IDUser"])['pseudo'];?> <span class="caret"></span> <span class="label">:)</span>
                        </a>
                        <div class="dropdown-menu">
                            <ul role="menu">
                                <li><a href="?page=profile&profile=edit">Profil <span class="badge pull-right bg-warning">13</span></a>
                                </li>
                                <li class="divider"></li>

                                <li><a href="?page=logOut">Déconnexion</a>
                                </li>

                            </ul>
                        </div>
                    </li>
                </ul>
                <?php
            }else{
                ?>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown dropdown-hover">
                        <a href="?page=login">Connexion</a>
                    </li>
                    <li>
                        <a href="?page=registration">Inscription</a>
                    </li>
                    <li>
                        <a class="search-toggle" href="search.html">
                            <i class="fa fa-search"></i>
                        </a>
                    </li>
                </ul>
                <?php

            }?>

        </div>
    </div>
</nav>
<!-- /Navbar -->
