
<!-- Preloader -->
<div class="page-preloader preloader-wrapp">
    <img src="assets/images/logo.png" alt="">
    <div class="preloader"></div>
</div>
<!-- /Preloader -->

<?php include ('view/components/navbar.php');?>
<!-- Main Content -->
<section class="content-wrap">

    <!-- Banner -->
    <div class="youplay-banner banner-top youplay-banner-parallax small">
        <div class="image" style="background-image: url('assets/images/banner-blog-bg.jpg')">    </div>

        <div class="youplay-user-navigation">
            <div class="container">
                <ul>
                    <li class="active"><a href="user-settings.html">Settings</a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="info">
            <div>
                <div class="container youplay-user">
                    <a href="<?= $user['url_photo']?>" class="angled-img image-popup">
                        <div class="img">
                            <img src="<?= $user['url_photo'] ?>" alt="">
                        </div>
                        <i class="fa fa-plus icon"></i>
                    </a>
                    <!--
                        -->
                    <div class="user-data">
                        <h2><?= $user['pseudo'] ?></h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Banner -->

    <div class="container youplay-content">

        <div class="row">

            <div class="col-md-12">
                <?php
                if (isset($_SESSION['success'])) { ?>
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong>Success!</strong><?= $_SESSION["success"] ?>
                    </div>
                    <?php
                    unset($_SESSION["success"]);
                }elseif (isset($_SESSION['errorPass'])) {
                    ?>
                    <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong>Danger!</strong><?= $_SESSION["errorPass"] ?>
                    </div>
                    <?php
                }
                unset($_SESSION["errorPass"]); ?>

                <form action="?page=profile&profile=editPass" method="post">
                    <h3>Change Password:</h3>
                    <div class="form-horizontal mt-30 mb-40">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="cur_password">Current Password:</label>
                            <div class="col-sm-10">
                                <div class="youplay-input">
                                    <input type="password"  name="cur_password" id="cur_password" placeholder="Current Password">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="new_password">New Password:</label>
                            <div class="col-sm-10">
                                <div class="youplay-input">
                                    <input type="password" name="new_password" id="new_password" placeholder="New Password">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default">Change Password</button>
                            </div>
                        </div>
                    </div>
                </form>
                <?php
                if (isset($_SESSION['successMail'])) { ?>
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong>Success!</strong><?= $_SESSION["successMail"] ?>
                    </div>
                    <?php
                    unset($_SESSION["successMail"]);
                }elseif (isset($_SESSION['errorMail'])) {
                    ?>
                    <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong>Danger!</strong><?= $_SESSION["errorMail"] ?>
                    </div>
                    <?php
                }
                unset($_SESSION["errorMail"]); ?>
                <form action="?page=profile&profile=editMail" method="post">
                    <h3>Email:</h3>
                    <div class="form-horizontal mt-30 mb-40">
                        <div class="form-group">
                            <label class="control-label col-sm-2">Current Email:</label>
                            <div class="col-sm-10">
                                <?= $user['email'] ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="new_email">New Email:</label>
                            <div class="col-sm-10">
                                <div class="youplay-input">
                                    <input type="email" id="new_email" name="new_email" placeholder="New Email">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default">Change Email</button>
                            </div>
                        </div>
                    </div>
                </form>
                <form id="submitFile" method="post" enctype="multipart/form-data">
                    <h3>Photo:</h3>
                    <div class="form-horizontal mt-30 mb-40">
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <label for="file" class="label-file"><img class="backWhite" src="<?= $user['url_photo'] ?>"></label><br>
                            <input id="file" class="input-file"  name="fileToUpload"  accept="image/x-png,image/gif,image/jpeg" type="file" onchange="document.getElementById('submitFile').submit(); ">                        </div>
                    </div></div>
                </form>
            </div>




        </div>

    </div>
</section>
