
<!-- Preloader -->
<div class="page-preloader preloader-wrapp">
    <img src="assets/images/logo.png" alt="">
    <div class="preloader"></div>
</div>
<!-- /Preloader -->
<?php include ('view/components/navbar.php');?>

<!-- Main Content -->
<section class="content-wrap">

    <!-- Banner -->
    <div class="youplay-banner banner-top youplay-banner-parallax xsmall">
        <div class="image" style="background-image: url('assets/images/banner-blog-bg.jpg')">
        </div>

        <div class="info">
            <div>
                <div class="container">
                    <h1>Forums</h1>
                </div>
            </div>
        </div>
    </div>
    <!-- /Banner -->


    <div class="container youplay-content">

        <div class="col-md-12">

            <!-- Breadcrumb -->
            <div class="mt-10 pull-left">
                <a href="index-2.html">Home</a>
                <span class="fa fa-angle-right"></span>
                <span>Forums</span>
            </div>
            <!-- /Breadcrumb -->

            <!-- Search -->
            <form action="http://html.nkdev.info/youplay/dark/search.html" class="pull-right">
                <p>Search by Forum:</p>
                <div class="youplay-input pull-left">
                    <input type="text" name="search">
                </div>
                <button class="btn pull-right">Search</button>
            </form>
            <!-- /Search -->

            <div class="clearfix"></div>

            <!-- Forums List -->
            <ul class="youplay-forum mr-10">
                <li class="header">
                    <ul>
                        <li class="cell-icon"></li>
                        <li class="cell-info">Forum</li>
                        <li class="cell-reply-count">Sous-Catégories</li>
                    </ul>
                </li>

                <li class="body">
                    <?php foreach (Pdog6::getCategory() as $cat){
                        ?>
                        <ul>
                            <li class="cell-icon">
                                <i class="fa fa-folder-open-o"></i>
                            </li>
                            <li class="cell-info">
                                <a href="?page=topic&cat=<?= $cat["id_theme"]?>" class="title h4"><?= $cat["category"]; ?></a>
                                <div class="description">Games for different consoles</div>
                                <ul class="forums-list">
                                    <?php
                                    foreach (Pdog6::getSubCategory($cat["id_theme"]) as $sub){
                                    ?>
                                        <li><a href=""><i class="fa fa-folder-open-o"></i> <?= $sub["nom"]; ?> </a>,</li>
                                    <?php
                                    }
                                    ?>
                                    </li>
                                </ul>
                            </li>
                            <li class="cell-reply-count"><?= Pdog6::getNbrSub($cat["id_theme"]);?></li>

                        </ul>
                    <?php
                    }
                    ?>

                </li>

            </ul>
            <!-- /Forums List -->

            <div class="clearfix"></div>
        </div>
        <!-- /Right Side -->

    </div>