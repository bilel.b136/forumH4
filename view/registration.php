
<!-- Preloader -->
<div class="page-preloader preloader-wrapp">
    <img src="assets/images/logo.png" alt="">
    <div class="preloader"></div>
</div>
<!-- /Preloader -->



<!-- Main Content -->
<section class="content-wrap full youplay-login">

    <!-- Banner -->
    <div class="youplay-banner banner-top">
        <div class="image" style="background-image: url('assets/images/banner-bg.jpg')"></div>

        <div class="info">
            <div>
                <div class="container align-center">
                    <div class="youplay-form">
                        <h1 style="font-style: italic">Inscription</h1>
                        <?php
                        if (isset($_SESSION['error'])){
                            ?>
                            <div class="alert alert-danger">
                                <strong>Danger!</strong> <?= $_SESSION['error']?>
                            </div>
                            <?php
                            unset($_SESSION['error']);
                        }

                        ?>
                        <form action="?page=registration&profile=postreg" method="post">
                            <div class="youplay-input">
                                <input type="text" name="email" placeholder="Email">
                            </div>
                            <div class="youplay-input">
                                <input type="text" name="pseudo" placeholder="Pseudo">
                            </div>
                            <div class="youplay-input">
                                <input type="password" name="password" placeholder="Password">
                            </div>
                            <button class="btn btn-default db">Registration</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Banner -->
</section>
<!-- /Main Content -->

<!-- Search Block -->
<div class="search-block">
    <a href="#!" class="search-toggle glyphicon glyphicon-remove"></a>
    <form action="http://html.nkdev.info/youplay/dark/search.html">
        <div class="youplay-input">
            <input type="text" name="search" placeholder="Search...">
        </div>
    </form>
</div>
<!-- /Search Block -->
